
int LED1 = 2; // LED on pin GPI02
int DELAY_INT = 12;

void setup() {
  // put your setup code here, to run once:
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
//  for (int i = 0; i < 5; i++) {
    digitalWrite(LED_BUILTIN, LOW);
    delay(DELAY_INT);
    digitalWrite(LED_BUILTIN, HIGH);
    delay(DELAY_INT);
//  }
}
