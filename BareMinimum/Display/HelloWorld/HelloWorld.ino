#include <MD_Parola.h>
#include <MD_MAX72xx.h>
#include <SPI.h>

#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES 4

#define CLK_PIN     14 // D5 on Arduino and SCLK on display
#define DATA_PIN    13 // D7 on Arduino and MOSI on display
#define CS_PIN      15 // D8 on Arduino and CS on display

MD_Parola P = MD_Parola(HARDWARE_TYPE, CS_PIN, MAX_DEVICES);

void setup(void)
{
  P.begin();
}

void loop(void)
{
  if (P.displayAnimate())
    P.displayScroll("Hello world!", PA_LEFT, PA_SCROLL_LEFT, 40);
  //  P.displayText("Hello", PA_CENTER, 200, 200, PA_SCROLL_DOWN, PA_SCROLL_UP);
   

}