#include "sensor_Name.h"

// Функция вывода адреса датчика 
void printAddress(DeviceAddress deviceAddress)
{
  for (uint8_t i = 0; i < 8; i++)
  {
    if (deviceAddress[i] < 16) Serial.print("0");
    Serial.print(deviceAddress[i], HEX); // Выводим адрес датчика в HEX формате 
  }
}
