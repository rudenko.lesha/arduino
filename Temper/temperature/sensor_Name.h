#ifndef SENSOR_NAME_H
#define SENSOR_NAME_H
#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>

void printAddress(DeviceAddress deviceAddress);
#endif
