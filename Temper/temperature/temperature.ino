#include "sensor_Name.h"

#define ONE_WIRE_BUS 2 // Пин подключения OneWire шины, 0 (D4)
OneWire oneWire(ONE_WIRE_BUS); // Подключаем бибилотеку OneWire
DallasTemperature sensors(&oneWire); // Подключаем бибилотеку DallasTemperature

DeviceAddress temperatureSensors[2]; // Размер массива определяем исходя из количества установленных датчиков
uint8_t deviceCount = 0;


void setup(void)
{
  Serial.begin(9600); // Задаем скорость соединения с последовательным портом 
    while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  sensors.begin(); // Иницилизируем датчики
  deviceCount = sensors.getDeviceCount(); // Получаем количество обнаруженных датчиков

  for (uint8_t index = 0; index < deviceCount; index++)
  {
    sensors.getAddress(temperatureSensors[index], index);
  }
}

void loop(void)
{
  Serial.println();
  sensors.requestTemperatures();
  for (int i = 0; i < deviceCount; i++)
  {
    printAddress(temperatureSensors[i]); // Выводим название датчика
    Serial.print(": ");
    Serial.println(sensors.getTempC(temperatureSensors[i])); // Выводим температуру с датчика
  }
  delay(10000);
}
